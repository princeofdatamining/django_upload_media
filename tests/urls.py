from django.conf.urls import url, include
urlpatterns = (
    url(r'^api/upload/', include('upload_media.restful.v1')),
)
