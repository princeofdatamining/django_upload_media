# Django Restful Upload

[![Build Status](https://travis-ci.org/princeofdatamining/django_upload_media.svg?branch=master)](https://travis-ci.org/princeofdatamining/django_upload_media)

Current version: `1.0.1`

This project makes it easy to upload binary:

* support `multipart/form-data` mode;
* support `application/json`(base64 encode binary before upload);


## Supported Django Versions

For Django `1.9.x` or higher, use the latest version. 


## Installation

1. `$ pip install git+https://github.com/princeofdatamining/django_upload_media`


## Configuration

1. Add `django_upload_media` to your `INSTALLED_APPS`.


2. Add URL include to your project's ``urls.py`` file::

    ```
    (r'^upload/', include('django_upload_media.restful.v1')),
    ```


## Demo

    POST {
        "base64": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAQAAAADpb+tAAAAJUlEQVQIW2NgQAf/MUSgghgS/9FoFJUYglhVUlsQxMXh1v+YggDKvxfpwg9anQAAAABJRU5ErkJggg=="
    }
