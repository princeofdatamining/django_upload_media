from django.utils.translation import ugettext_lazy as _, ungettext_lazy, pgettext_lazy, npgettext_lazy

APP_VERBOSE_NAME = pgettext_lazy('APP', 'upload_media')

MODEL_UPLOAD = pgettext_lazy('Model', 'Upload file')
MODEL_UPLOADS = pgettext_lazy('Model', 'Upload files')

USER = _('user')
TIME_CREATE = pgettext_lazy('Upload', 'create_time')
CATEGORY = pgettext_lazy('Upload', 'category')
MEDIA = pgettext_lazy('Upload', 'file')
FILENAME = pgettext_lazy('Upload', 'filename')
FILESIZE = pgettext_lazy('Upload', 'filesize')
CHECKSUM = pgettext_lazy('Upload', 'checksum')
WIDTH = pgettext_lazy('Upload', 'width')
HEIGHT = pgettext_lazy('Upload', 'height')
