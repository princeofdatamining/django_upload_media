from rest_framework import fields
from django.core.files import uploadedfile
from django.utils import timezone
import io
import base64


# submit json 时，image/file 是BASE64过的文本串
def convert_base64(data, name):
    # data:image/jpeg;base64,.....
    meta, sep, binary = data.partition(';base64,')
    if not sep:
        return 
    binary = base64.b64decode(binary.encode('ascii'))
    content_type = meta.lstrip('data:')
    mime_type, sep, extension = content_type.partition('/')
    #
    return uploadedfile.InMemoryUploadedFile(
        io.BytesIO(binary),
        name,
        timezone.now().strftime('%Y-%m-%d_%H%M%S_%f') + '.' + extension,
        content_type, len(binary), None, {}
    )


# 抛出 SkipField 可以不让该 field 设置到 validated_data 中
'''
        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            ...
            except SkipField:
                pass
            else:
                set_value(ret, field.source_attrs, validated_value)
'''
def raise_skip(*args, **kwargs):
    raise fields.SkipField()


class Base64FileField(fields.FileField):

    def to_internal_value(self, data):
        data = convert_base64(data, self.source)
        return super(Base64FileField, self).to_internal_value(data)

    raise_skip = False
    def run_validation(self, data=fields.empty):
        if self.raise_skip:
            raise fields.SkipField()
        return super(Base64FileField, self).run_validation(data)


class Base64ImageField(fields.ImageField):

    def to_internal_value(self, data):
        data = convert_base64(data, self.source)
        return super(Base64ImageField, self).to_internal_value(data)

    raise_skip = False
    def run_validation(self, data=fields.empty):
        if self.raise_skip:
            raise fields.SkipField()
        return super(Base64ImageField, self).run_validation(data)
