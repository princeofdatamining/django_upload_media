from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from django.conf import settings
from django.core.urlresolvers import get_callable


try:
    APITestCase = get_callable(settings.RESTFUL_TESTCASE)
except:
    pass


class TestUpload(APITestCase):

    def setUp(self):
        self.url = self.reverse('api-upload:upload_media')

    def check_response(self, resp):
        self.assertEqual(201, resp.status_code)
        # self.assertEqual(94, resp.data.get('filesize'))
        # self.assertEqual("4142f94673430980539f76c5248467b0", resp.data.get('checksum'))
        # self.assertEqual(11, resp.data.get('width'))

    def test_base64(self):
        resp = self.client.post(self.url, {
            'base64': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAALCAQAAAADpb+tAAAAJUlEQVQIW2NgQAf/MUSgghgS/9FoFJUYglhVUlsQxMXh1v+YggDKvxfpwg9anQAAAABJRU5ErkJggg==',
        })
        self.check_response(resp)

    def test_multiform(self):
        pass
