from rest_framework import generics
from rest_framework import permissions
from rest_framework import serializers

from upload_media import models, constant
from upload_media.restful import fields


class UploadSerializer(serializers.ModelSerializer):

    user = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = models.Upload
        fields = ('media',
            'create_time', 'user', 'category',
            'filename', 'filesize', 'checksum', 'width', 'height')


class UploadSubmitSerializer(serializers.ModelSerializer):

    base64 = fields.Base64FileField(source='media')

    class Meta:
        model = models.Upload
        fields = ('media', 'base64',)

    # 既支持 "multipart/form-data" 又支持 "application/json"
    # 指定 base64,适用于"application/json"；
    # 如要使用"multipart/form-data"时，一定不能指定 base64;
    def to_internal_value(self, data):
        media, base64 = self.fields['media'], self.fields['base64']
        if data.get('base64'):
            media.run_validation = fields.raise_skip
        else:
            base64.raise_skip = True
        return super(UploadSubmitSerializer, self).to_internal_value(data)

    def save(self, **kwagrs):
        # 自动填写 upload user
        kwagrs.setdefault('user_id', self.context['request'].user.pk)
        return super(UploadSubmitSerializer, self).save(**kwagrs)


class UploadsView(generics.ListCreateAPIView):

    queryset = models.Uploads.none()
    submit_serializer_class = UploadSubmitSerializer
    serializer_class = UploadSerializer

    def get_serializer_class(self, *args, **kwagrs):
        if self.request.method == 'GET':
            return self.serializer_class
        else:
            return self.submit_serializer_class

#

from django.conf import urls
urlpatterns = [
    urls.url(r'^$', UploadsView.as_view(), name='upload_media'),
]

import rest_framework.urlpatterns
urlpatterns = rest_framework.urlpatterns.format_suffix_patterns(urlpatterns)
