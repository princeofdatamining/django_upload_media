
VERSION = (1, 0, 2, 2017, 6, 16, '')
__version__ = "1.0.2"
__release__ = "17.06.16.1"

default_app_config = 'upload_media.apps.QAppConfig'
