from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.utils import timezone
from django.conf import settings
from django.core.files.images import get_image_dimensions

import os
import hashlib

from . import constant

USER = settings.AUTH_USER_MODEL
UPLOAD_SETTINGS = getattr(settings, 'UPLOAD_SETTINGS', {})

class UploadManager(models.Manager):

    pass

Uploads = UploadManager()

def upload_file(instance, filename):
    # 文件大小、文件名
    instance.filesize = instance.media.size
    instance.filename = os.path.basename(filename)
    # 尝试获取 image 的 长、宽
    try:
        size = get_image_dimensions(instance.media)
    except:
        pass
    else:
        # IntegrityError: (1048, "Column 'width' cannot be null")
        w, h = size
        instance.width, instance.height = w or 0, h or 0
    # 保存校验码
    checksum = UPLOAD_SETTINGS.get('CHECKSUM_ALGORITHM')
    if checksum:
        file = instance.media.file
        H = hashlib.new(checksum)
        for data in file.chunks():
            H.update(data)
        instance.checksum = H.hexdigest()
    #
    prefix = UPLOAD_SETTINGS.get('PATH_PREFIX') or 'upload'
    time_format = UPLOAD_SETTINGS.get('TIME_FORMAT') or '%Y/%m-%d'
    return os.path.join(
        prefix,
        os.path.normpath(timezone.now().strftime(time_format)),
        instance.media.field.get_filename(filename),
    )

class Upload(models.Model):

    class Meta:
        verbose_name = constant.MODEL_UPLOAD
        verbose_name_plural = constant.MODEL_UPLOADS

    objects = Uploads

    user = models.ForeignKey(USER, verbose_name=constant.USER, null=True, blank=True, default=None)
    create_time = models.DateTimeField(constant.TIME_CREATE, null=True, blank=True, default=timezone.now)
    category = models.CharField(constant.CATEGORY, blank=True, default='', max_length=64)
    media = models.FileField(constant.MEDIA, upload_to=upload_file)
    filename = models.CharField(constant.FILENAME, max_length=191, blank=True, default='')
    filesize = models.IntegerField(constant.FILESIZE, blank=True, default=0)
    checksum = models.CharField(constant.CHECKSUM, max_length=191, blank=True, default='', db_index=True)
    width = models.IntegerField(constant.WIDTH, blank=True, default=0)
    height = models.IntegerField(constant.HEIGHT, blank=True, default=0)

    def __str__(self):
        return self.filename
